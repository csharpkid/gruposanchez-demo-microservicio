# Use an official Node.js runtime as a parent image
FROM node:lts-alpine3.19

# Set the working directory in the container
WORKDIR /usr/src/app


# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install app dependencies
RUN npm install

# Bundle app source
COPY . .


# Expose the port your app runs on
EXPOSE 7000


# Define the script to be executed when the container starts using the shell form of CMD to properly chain commands
CMD node app.js